//
//  Main.swift
//  PicPay
//
//  Created by Juliano Alvarenga on 10/07/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import UIKit
import CoreData

class Main: UIViewController {
    
    //MARK: - V A R I A B L E S
    @IBOutlet weak var newsTable        : UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let refreshControl              = UIRefreshControl()
    var request                     = URLRestAPI()
    var listOfNews                  = [ListOfNews]()
    var filteredNews                = [ListOfNews]()
    var persistedNews               = [PersistedNews]()
    var filteredPersistedNews       = [PersistedNews]()
    let imageCache                  = NSCache<NSString, UIImage>()
    var index                       : Int!
    var isSearching                 = false
    var isOffline                   = false
    var picker                      = UIPickerView()
    var image                       : UIImage!
    var fetchedResultsControllerNews: NSFetchedResultsController<PersistedNews>!
    
    //MARK: - L O A D I N G
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK: - P R I V A T E
    fileprivate func setupView() {
        creatingPickerFilter()
        setupSearchBar()                
        newsTable.register(NewsCell.nib, forCellReuseIdentifier: NewsCell.identifier)
        refreshControl.addTarget(self, action: #selector(refreshTable(_:)), for: .valueChanged)
        newsTable.refreshControl        = refreshControl
        getNewsOnline()
        setupFetchedResultsControllerNews()
        if Reachability.isConnectedToNetwork(){
            isOffline = false
        }else{
            isOffline = true            
        }
    }        
    
    @objc
    fileprivate func refreshTable(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            isOffline = false
            getListOfNews {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.newsTable.alpha = 1
                    })
                    self.newsTable.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        } else {
            isOffline = true
            setupFetchedResultsControllerNews()
            self.refreshControl.endRefreshing()
        }
        
    }
    
    fileprivate func reloadTable() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                self.newsTable.alpha            = 1
                self.activityIndicator.alpha    = 0
            })
            self.newsTable.reloadData()
        }
    }
    
    fileprivate func getNewsOnline() {
        getListOfNews {
            self.reloadTable()
        }
    }
    
    fileprivate func setupFetchedResultsControllerNews() {
        let fetcheRequest: NSFetchRequest<PersistedNews> = PersistedNews.fetchRequest()
        var persistenceService          = PersistenceService()
        let context                     = persistenceService.persistentContainer.viewContext
        let sortDescriptor              = NSSortDescriptor(key: "newsDate", ascending: true)
        fetcheRequest.sortDescriptors   = [sortDescriptor]
        fetchedResultsControllerNews    = NSFetchedResultsController(fetchRequest: fetcheRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: "news")
        fetchedResultsControllerNews.delegate = self
        
        do {
            try fetchedResultsControllerNews.performFetch()
            guard let news  = fetchedResultsControllerNews.fetchedObjects else { return }
            persistedNews   = news.sorted(by: {$0.newsDate! > $1.newsDate!})
            reloadTable()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    fileprivate func creatingPickerFilter() {
        picker = creatingPickerView(height: 120)
        picker.delegate = self
        view.addSubview(picker)
        picker.transform = CGAffineTransform.init(translationX: 0, y: 0)
    }
    
    fileprivate func setupSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.delegate                           = self
        searchController.searchBar.delegate                 = self
        searchController.searchBar.placeholder              = "Buscar"
        searchController.extendedLayoutIncludesOpaqueBars   = true
        searchController.dimsBackgroundDuringPresentation   = false
        self.definesPresentationContext = true
        navigationItem.searchController = searchController
    }
    
    //MARK: - A C T I O N S
    @IBAction func filterAction(_ sender: UIBarButtonItem) {        
        if sender.tag == 0 {
            sender.tag = 1
            UIView.animate(withDuration: 0.3) {
                self.picker.transform = CGAffineTransform.init(translationX: 0, y: -120)
            }
        } else {
            sender.tag = 0
            UIView.animate(withDuration: 0.3) {
                self.picker.transform = CGAffineTransform.init(translationX: 0, y: 0)
            }
        }
    }
}

//MARK: - E X T E N S I O N // S E A R C H
extension Main: UISearchControllerDelegate, UISearchBarDelegate  {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            isSearching = true
        } else {
            isSearching = false
        }
        if !isOffline {
            let filter      = listOfNews.filter({($0.titulo?.localizedStandardContains(searchText) ?? false)})
            filteredNews    = filter
        } else {
            let filter              = persistedNews.filter({($0.newsTitle?.localizedStandardContains(searchText) ?? false)})
            filteredPersistedNews   = filter
        }
        newsTable.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        newsTable.reloadData()
    }
}

//MARK: - E X T E N S I O N // P I C K E R
extension Main: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "Mais recentes"
        } else {
            return "Mais antigas"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            if isOffline {
                persistedNews   = persistedNews.sorted(by: {$0.newsDate! > $1.newsDate! })
            } else {
                listOfNews      = listOfNews.sorted(by: {$0.data ?? "" > $1.data ?? ""})
            }
        } else {
            if isOffline {
                persistedNews   = persistedNews.sorted(by: {$0.newsDate! < $1.newsDate! })
            } else {
                listOfNews      = listOfNews.sorted(by: {$0.data ?? "" < $1.data ?? ""})
            }
        }
        reloadAnimatedTable()
    }
    
    fileprivate func reloadAnimatedTable() {
        UIView.animate(withDuration: 0.1, animations: {
            self.newsTable.alpha = 0
        }) { (_) in
            self.newsTable.reloadData()
            UIView.animate(withDuration: 0.2, animations: {
                self.newsTable.alpha = 1
            })
        }
    }
    
}

extension Main: NSFetchedResultsControllerDelegate {}
