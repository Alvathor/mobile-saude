//
//  TableView.swift
//  Mobile
//
//  Created by Juliano Alvarenga on 11/07/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import Foundation
import UIKit

extension Main: UITableViewDelegate, UITableViewDataSource {
    
    fileprivate func returnListOfNewsPersisted() -> [PersistedNews] {
        if isSearching {
            return filteredPersistedNews
        } else {
            return persistedNews
        }
    }
    
    fileprivate func returnListOfNewsOnline() -> [ListOfNews] {
        if isSearching {
            return filteredNews
        } else {
            return listOfNews
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isOffline {
            return returnListOfNewsOnline().count
        } else {
            return returnListOfNewsPersisted().count
        }
        
    }
    
    fileprivate func buildCellUsinOnlineData(_ indexPath: IndexPath, _ cell: NewsCell, _ tableView: UITableView) {
        let list        = returnListOfNewsOnline()
        let urlImage    = list[indexPath.row].foto_principal_url ?? ""
        cell.activityIndicator.startAnimating()
        requestImage(urlImage: urlImage) { (image) in
            DispatchQueue.main.async {
                if let updateCell = tableView.cellForRow(at: indexPath) as? NewsCell {
                    updateCell.profilePic.image = image
                    updateCell.activityIndicator.stopAnimating()
                }
            }
        }
        cell.constructingCell(news: list[indexPath.row])
    }
    
    fileprivate func buildCellUsingPersistedData(_ indexPath: IndexPath, _ cell: NewsCell, _ tableView: UITableView) {
        let list    = returnListOfNewsPersisted()
        cell.constructingCellFromPersistence(news: list[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell        = tableView.dequeueReusableCell(withIdentifier: NewsCell.identifier, for: indexPath) as! NewsCell
        if !isOffline {
            buildCellUsinOnlineData(indexPath, cell, tableView)
        } else {
            buildCellUsingPersistedData(indexPath, cell, tableView)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {        
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell    = tableView.cellForRow(at: indexPath) as! NewsCell
        index       = indexPath.row
        image       = cell.profilePic.image
        performSegue(withIdentifier: "toNewsDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination           = segue.destination as? NewsTVC else { return }
        destination.image               = image
        if !isOffline {
            destination.isOffline       = false
            destination.news            = returnListOfNewsOnline()
        } else {
            destination.isOffline       = true
            destination.persistedNews   = returnListOfNewsPersisted()
        }
        destination.index               = index
    }    
}
