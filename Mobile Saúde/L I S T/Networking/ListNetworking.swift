//
//  ListNetworking.swift
//  Mobile
//
//  Created by Juliano Alvarenga on 07/12/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import Foundation
import UIKit

extension Main {
    
    func getListOfNews(completion: @escaping() -> ()) {
        let url = URL.init(string: "http://mobilesaude.com.br/challenge/lista.json")!
        newsTable.alpha = 0
        statusActivityIndicator(activity: activityIndicator, status: .start)
        request.serviceAPI(method: .get, url: url, json: nil, activityIndicator: activityIndicator) {[weak self] (resp: [ListOfNews]) in
            self?.listOfNews = resp.sorted(by: {$0.data ?? "" > $1.data ?? ""})
            completion()
            self?.statusActivityIndicator(activity: self?.activityIndicator, status: .stop)
        }
    }
    
    func requestImage(urlImage: String, completion: @escaping(UIImage?) -> ()) {
        let url                 = URL.init(string: urlImage)!
        let urlFromCache        = url.absoluteString
        if let imageFromCache   = self.imageCache.object(forKey: url.absoluteString as NSString) {
            completion(imageFromCache)
            return
        } else {
            URLSession.shared.dataTask(with: url) { [weak self] (data, resp, error) in
                guard error     == nil else { return }
                guard let data  = data else { return }
                guard let image = UIImage(data: data) else { return }
                if urlFromCache == url.absoluteString {
                    completion(image)
                }
                self?.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                }.resume()
        }
    }
}
