//
//  Users.swift
//  PicPay
//
//  Created by Juliano Alvarenga on 11/07/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import Foundation

import Foundation
struct ListOfNews : Codable {
    let id                  : Int?
    let titulo              : String?
    let conteudo            : String?
    let sumario             : String?
    let data                : String?
    let data_validade       : String?
    let data_publicacao     : String?
    let formato             : Int?
    let cod_operadora       : Int?
    let foto_principal_url  : String?
    let permalink           : String?
    let post_tipo_id        : Int?
    let categorias          : [Int]?
    let categorias_nomes    : [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case id                     = "id"
        case titulo                 = "titulo"
        case conteudo               = "conteudo"
        case sumario                = "sumario"
        case data                   = "data"
        case data_validade          = "data_validade"
        case data_publicacao        = "data_publicacao"
        case formato                = "formato"
        case cod_operadora          = "cod_operadora"
        case foto_principal_url     = "foto_principal_url"
        case permalink              = "permalink"
        case post_tipo_id           = "post_tipo_id"
        case categorias             = "categorias"
        case categorias_nomes       = "categorias_nomes"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id                  = try values.decodeIfPresent(Int.self,      forKey: .id)
        titulo              = try values.decodeIfPresent(String.self,   forKey: .titulo)
        conteudo            = try values.decodeIfPresent(String.self,   forKey: .conteudo)
        sumario             = try values.decodeIfPresent(String.self,   forKey: .sumario)
        data                = try values.decodeIfPresent(String.self,   forKey: .data)
        data_validade       = try values.decodeIfPresent(String.self,   forKey: .data_validade)
        data_publicacao     = try values.decodeIfPresent(String.self,   forKey: .data_publicacao)
        formato             = try values.decodeIfPresent(Int.self,      forKey: .formato)
        cod_operadora       = try values.decodeIfPresent(Int.self,      forKey: .cod_operadora)
        foto_principal_url  = try values.decodeIfPresent(String.self,   forKey: .foto_principal_url)
        permalink           = try values.decodeIfPresent(String.self,   forKey: .permalink)
        post_tipo_id        = try values.decodeIfPresent(Int.self,      forKey: .post_tipo_id)
        categorias          = try values.decodeIfPresent([Int].self,    forKey: .categorias)
        categorias_nomes    = try values.decodeIfPresent([String].self, forKey: .categorias_nomes)
    }
    
}

