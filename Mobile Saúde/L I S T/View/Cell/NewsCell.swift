//
//  NewsCell.swift
//  Mobile
//
//  Created by Juliano Alvarenga on 11/07/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    
    //MARK: - V A R I A B L E S
    @IBOutlet weak var profilePic           : UIImageView!
    @IBOutlet weak var nameLabel            : UILabel!
    @IBOutlet weak var dateLabel            : UILabel!
    @IBOutlet weak var activityIndicator    : UIActivityIndicatorView!        
    
    //MARK: - L O A D I N G
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }


    override func prepareForReuse() {
        profilePic.image = nil
        nameLabel.text?.removeAll()
        dateLabel.text?.removeAll()
    }
    
    //MARK: - F U N C T I O N S
    fileprivate func setupView() {
        profilePic.layer.cornerRadius   = profilePic.frame.size.width / 2
        profilePic.layer.masksToBounds  = true
    }
       
    func constructingCell(news: ListOfNews) {
        let masks               = Masks()        
        nameLabel.text          = news.titulo
        dateLabel.text          = masks.formattedDateSync(_date: news.data)
    }
    
    func constructingCellFromPersistence(news: PersistedNews) {        
        nameLabel.text      = news.newsTitle        
        dateLabel.text      = news.newsDate
        profilePic.image    = UIImage(data: news.newsImage!)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
