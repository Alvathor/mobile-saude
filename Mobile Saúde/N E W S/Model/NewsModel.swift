//
//  NewsModel.swift
//  Mobile Saúde
//
//  Created by Juliano Alvarenga on 11/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import Foundation

import Foundation
struct NewsModel : Codable {
    let id                  : Int?
    let titulo              : String?
    let conteudo            : String?
    let data                : String?
    let data_validade       : String?
    let idioma              : String?
    let cod_operadora       : String?
    let foto_principal_url  : String?
    let permalink           : String?
    let post_tipo_id        : Int?
    let status              : String?
    let width_exibicao      : String?
    let height_exibicao     : String?
    let tipo_exibicao       : String?
    let exibir_titulo       : String?
    let podcast             : String?
    let video               : String?
    let autor               : String?
    let formato             : Int?
    let ambiente            : String?
    let categorias          : [Int]?
    let tags                : [String]?
    let galeria_fotos       : String?
    let galeria_arquivos    : String?
    let sanfona             : [String]?
    let categorias_nomes    : [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case id                     = "id"
        case titulo                 = "titulo"
        case conteudo               = "conteudo"
        case data                   = "data"
        case data_validade          = "data_validade"
        case idioma                 = "idioma"
        case cod_operadora          = "cod_operadora"
        case foto_principal_url     = "foto_principal_url"
        case permalink              = "permalink"
        case post_tipo_id           = "post_tipo_id"
        case status                 = "status"
        case width_exibicao         = "width_exibicao"
        case height_exibicao        = "height_exibicao"
        case tipo_exibicao          = "tipo_exibicao"
        case exibir_titulo          = "exibir_titulo"
        case podcast                = "podcast"
        case video                  = "video"
        case autor                  = "autor"
        case formato                = "formato"
        case ambiente               = "ambiente"
        case categorias             = "categorias"
        case tags                   = "tags"
        case galeria_fotos          = "galeria_fotos"
        case galeria_arquivos       = "galeria_arquivos"
        case sanfona                = "sanfona"
        case categorias_nomes       = "categorias_nomes"
    }
    
    init(from decoder: Decoder) throws {
        let values              = try decoder.container(keyedBy: CodingKeys.self)
        id                      = try values.decodeIfPresent(Int.self,      forKey: .id)
        titulo                  = try values.decodeIfPresent(String.self,   forKey: .titulo)
        conteudo                = try values.decodeIfPresent(String.self,   forKey: .conteudo)
        data                    = try values.decodeIfPresent(String.self,   forKey: .data)
        data_validade           = try values.decodeIfPresent(String.self,   forKey: .data_validade)
        idioma                  = try values.decodeIfPresent(String.self,   forKey: .idioma)
        cod_operadora           = try values.decodeIfPresent(String.self,   forKey: .cod_operadora)
        foto_principal_url      = try values.decodeIfPresent(String.self,   forKey: .foto_principal_url)
        permalink 				= try values.decodeIfPresent(String.self,   forKey: .permalink)
        post_tipo_id            = try values.decodeIfPresent(Int.self,      forKey: .post_tipo_id)
        status 					= try values.decodeIfPresent(String.self,   forKey: .status)
        width_exibicao 			= try values.decodeIfPresent(String.self,   forKey: .width_exibicao)
        height_exibicao 		= try values.decodeIfPresent(String.self,   forKey: .height_exibicao)
        tipo_exibicao 			= try values.decodeIfPresent(String.self,   forKey: .tipo_exibicao)
        exibir_titulo 			= try values.decodeIfPresent(String.self,   forKey: .exibir_titulo)
        podcast                 = try values.decodeIfPresent(String.self,   forKey: .podcast)
        video                   = try values.decodeIfPresent(String.self,   forKey: .video)
        autor                   = try values.decodeIfPresent(String.self,   forKey: .autor)
        formato                 = try values.decodeIfPresent(Int.self,      forKey: .formato)
        ambiente                = try values.decodeIfPresent(String.self,   forKey: .ambiente)
        categorias              = try values.decodeIfPresent([Int].self,    forKey: .categorias)
        tags                    = try values.decodeIfPresent([String].self, forKey: .tags)
        galeria_fotos           = try values.decodeIfPresent(String.self,   forKey: .galeria_fotos)
        galeria_arquivos        = try values.decodeIfPresent(String.self,   forKey: .galeria_arquivos)
        sanfona                 = try values.decodeIfPresent([String].self, forKey: .sanfona)
        categorias_nomes        = try values.decodeIfPresent([String].self, forKey: .categorias_nomes)
    }
    
}
