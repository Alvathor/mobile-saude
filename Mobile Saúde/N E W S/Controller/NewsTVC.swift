//
//  NewsTVC.swift
//  Mobile Saúde
//
//  Created by Juliano Alvarenga on 10/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit
import CoreData

class NewsTVC: UITableViewController {

    //MARK: - V A R I A B L E S
    @IBOutlet var newsCells                 : [UITableViewCell]!
    @IBOutlet weak var newsImageImageView   : UIImageView!
    @IBOutlet weak var newsTitleLabel       : UILabel!
    @IBOutlet weak var newsTextView         : UITextView!
    @IBOutlet weak var activityIndicator    : UIActivityIndicatorView!
    
    var news                        : [ListOfNews]!
    var persistedNews               : [PersistedNews]!
    var image                       : UIImage!
    var isOffline                   : Bool!
    var index                       : Int!
    var fetchedResultsControllerNews: NSFetchedResultsController<PersistedNews>!
    var persistenceService          = PersistenceService()
    
    //MARK: - L O A D I N G
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    //MARK: - P R I V A T E
    fileprivate func setupView() {
        setupFetchedResultsControllerNews()
        let masks = Masks()
        if isOffline {
            navigationItem.rightBarButtonItem?.image = UIImage(named: "filledStar")
            navigationItem.rightBarButtonItem?.tag = 1
            newsTitleLabel.text         = persistedNews[index].newsTitle
            newsTextView.text           = persistedNews[index].newsContent
            newsImageImageView.image    = image
            navigationItem.title        = persistedNews[index].newsDate
        } else {
            for _news in persistedNews {
                if Int(_news.newsId) == news[index].id  {
                    navigationItem.rightBarButtonItem?.image = UIImage(named: "filledStar")
                    navigationItem.rightBarButtonItem?.tag = 1
                    break
                } else {
                    navigationItem.rightBarButtonItem?.image = UIImage(named: "emptyStar")
                    navigationItem.rightBarButtonItem?.tag = 0
                }
            }
            newsTitleLabel.text         = news[index].titulo
            newsTextView.text           = news[index].conteudo
            newsImageImageView.image    = image
            navigationItem.title        = masks.formattedDateSync(_date: news[index].data)
        }
        
    }
    
    //MARK: - P E R S I S T E C E // C O R E  D A T A
    fileprivate func setupFetchedResultsControllerNews() {
        let fetcheRequest: NSFetchRequest<PersistedNews> = PersistedNews.fetchRequest()
        let context                     = persistenceService.persistentContainer.viewContext
        let sortDescriptor              = NSSortDescriptor(key: "newsDate", ascending: true)
        fetcheRequest.sortDescriptors   = [sortDescriptor]
        fetchedResultsControllerNews    = NSFetchedResultsController(fetchRequest: fetcheRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: "news")
        fetchedResultsControllerNews.delegate = self
        
        do {
            try fetchedResultsControllerNews.performFetch()
            guard let news  = fetchedResultsControllerNews.fetchedObjects else { return }
            persistedNews   = news
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
    fileprivate func savingNewsAsFavorite() {
        let _news = PersistedNews(context: persistenceService.persistentContainer.viewContext)
        _news.newsTitle      = newsTitleLabel.text
        _news.newsContent    = newsTextView.text
        _news.newsId         = Int16(news[index].id!)
        let mask             = Masks()
        _news.newsDate       = mask.formattedDateSync(_date: news[index].data)
        if let img           = image {
            let data         = img.pngData()
            _news.newsImage  = data
        }
        try? persistenceService.persistentContainer.viewContext.save()
        navigationItem.rightBarButtonItem?.image = UIImage(named: "filledStar")
    }
    
    fileprivate func removeNewsFromFavorite() {
        guard let favoriteNews  = fetchedResultsControllerNews.fetchedObjects else { return }
        for _news in favoriteNews {
            if isOffline {
                persistenceService.persistentContainer.viewContext.delete(_news)
                break
            } else {
                if Int(_news.newsId) == news[index].id {
                    persistenceService.persistentContainer.viewContext.delete(_news)
                    break
                }
            }
        }
        try? persistenceService.persistentContainer.viewContext.save()
        navigationItem.rightBarButtonItem?.image = UIImage(named: "emptyStar")
    }
    
    // MARK: - T A B L E
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsCells.count
    }
    
    // MARK: - A C T I O N S
    @IBAction func saveNewsAction(_ sender: UIBarButtonItem) {
        if navigationItem.rightBarButtonItem?.tag == 0 {
            savingNewsAsFavorite()
        } else {
            removeNewsFromFavorite()
        }
    }
}

// MARK: - E X T E N S I O N S
extension NewsTVC: NSFetchedResultsControllerDelegate {}
