//
//  Alerts.swift
//  PicPay
//
//  Created by Juliano Alvarenga on 05/06/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import UIKit

struct Alerts {
    
    func simpleAlert(passedTitle: String, passedMessage: String, completion: @escaping() -> ()) {
        let controller = UIAlertController(title: passedTitle, message: passedMessage, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            completion()
        }))
        DispatchQueue.main.async {
            guard let rootController = UIApplication.shared.keyWindow?.visibleViewController else { return }
            rootController.present(controller, animated: true, completion: nil)
        }
    }
}

extension UIViewController {
    
    func confirmationAlert(externalController: UIViewController, passedTitle: String, passedMessage: String, completion: @escaping() -> ()) {
        
        let controller = UIAlertController(title: passedTitle, message: passedMessage, preferredStyle: .alert)
        
        
        controller.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: nil))
        controller.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { (_) in
            
            completion()
        }))
        
        DispatchQueue.main.async {
            
            externalController.present(controller, animated: true, completion: nil)
        }
        
    }
    
    func alertCadastro(externalController: UIViewController) {
        
        simpleAlert(passedTitle: "", passedMessage: "Favor preencher corretamente os campos", completion:{})
        
    }
    
    func alertBuy(externalController: UIViewController, completion: @escaping() -> ()) {
        
        simpleAlert(passedTitle: "Nova aplicação", passedMessage: "Aplicação efetuada com sucesso.") {
            
            completion()
        }
    }
    
    func alertSell(externalController: UIViewController, completion: @escaping() -> ()) {
        
        simpleAlert(passedTitle: "Resgate de título", passedMessage: "Resgate efetuada com sucesso.") {
            
            completion()
        }
    }
    
    //MARK: - Alert Connection
    
    func simpleAlert(passedTitle: String, passedMessage: String, completion: @escaping() -> ()) {
        let controller = UIAlertController(title: passedTitle, message: passedMessage, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            completion()
        }))
        DispatchQueue.main.async {
            guard let rootController = UIApplication.shared.windows.first?.visibleViewController else { return }
            rootController.present(controller, animated: true, completion: nil)
        }
    }
    
    func alert(completion: @escaping() -> ()) {
                
        DispatchQueue.main.async {
            
            self.simpleAlert( passedTitle: "Atenção", passedMessage: "Não foi possível obter informações, por favor verifique sua conexão", completion: {
                
                completion()
            })            
        }
    }
    
    //MARK: - AlertSell

    fileprivate func configurationTextField(textField: UITextField, controller: UIViewController) {
        
        textField.placeholder = "Senha"
        textField.isSecureTextEntry = true
        textField.keyboardType = .numberPad
        textField.textAlignment = .center
        textField.delegate = controller as? UITextFieldDelegate
    }
    
}

enum typeTax: Int {
    
    case iof = 1
    case ir = 2
}


public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(vc: self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(vc: nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(vc: tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: pvc)
            } else {
                return vc
            }
        }
    }
}

