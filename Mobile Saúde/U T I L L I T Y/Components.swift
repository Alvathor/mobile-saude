//
//  Components.swift
//  Mobile Saúde
//
//  Created by Juliano Alvarenga on 10/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func creatingPickerView(height: CGFloat) -> UIPickerView {
        let picker: UIPickerView
        let frame = CGRect(x: 0, y: view.frame.height, width: view.frame.width, height: height)
        picker = UIPickerView(frame: frame)
        picker.backgroundColor          = .white
        picker.showsSelectionIndicator  = true
        
        return picker
    }
}
