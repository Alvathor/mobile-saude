//
//  Numbers.swift
//  PicPay
//
//  Created by Juliano Alvarenga on 06/12/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import Foundation

struct FormatCurrency {
    
    func formatCurrency(_ value: String) -> String {
        let cents = value.suffix(2)
        
        if value.count > 2 {
            
            let final = value.prefix(value.count - cents.count)
            let myDouble = Double(final)!
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = .currency
            currencyFormatter.maximumFractionDigits = 0
            
            currencyFormatter.locale = Locale(identifier: "pt_BR")
            let priceString = currencyFormatter.string(from: NSNumber(value: myDouble))!
            
            return "\(priceString),\(cents)"
        } else {
            return "R$ 0,\(value)"
        }
    }
    
    func cleanCurrency(_ value: String) -> String {
        let cents = value.suffix(2)
        let finalValue = value.prefix(value.description.count - cents.description.count)
        return "\(finalValue).\(cents)"
    }
}
