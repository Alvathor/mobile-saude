//
//  PassData.swift
//  Mobile Saúde
//
//  Created by Juliano Alvarenga on 11/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import Foundation

protocol AbleToReceiveData: class {
    func passData<T>(data: T)
}
