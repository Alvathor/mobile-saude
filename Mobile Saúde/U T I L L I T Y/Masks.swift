//
//  Masks.swift
//  Mobile Saúde
//
//  Created by Juliano Alvarenga on 11/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import Foundation

struct Masks {
    func formattedDateSync(_date: String?) -> String {
        let dateFormatterGet = DateFormatter()
        let dateFormatterPrint = DateFormatter()
        guard let innerDate = _date else { return "" }
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        dateFormatterPrint.dateFormat = "dd/MM/yyyy"
        dateFormatterPrint.locale   = Locale(identifier: "pt-br")
        guard let date = dateFormatterGet.date(from: innerDate) else { return "" }
        
        return dateFormatterPrint.string(from: date)
    }
    
    func formattedReturnDate(_date: String?) -> Date {
        let dateFormatterGet = DateFormatter()
        let dateFormatterPrint = DateFormatter()
        guard let innerDate = _date else { return Date.init() }
        dateFormatterGet.dateFormat     = "dd/MM/yyyy"
        dateFormatterPrint.dateFormat   = "yyyy-MM-dd HH:mm:ss"
        guard let date = dateFormatterGet.date(from: innerDate) else { return Date.init() }
        
        return date
    }
}
