//
//  ActivityIndicator.swift
//  PicPay
//
//  Created by Juliano Alvarenga on 07/12/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func statusActivityIndicator(activity: UIActivityIndicatorView?, status: StatusActivity) {
        switch status {
        case .start:
            DispatchQueue.main.async {
                activity?.startAnimating()
            }
        case .stop:
            DispatchQueue.main.async {
                activity?.stopAnimating()
            }
        }
    }
}

enum StatusActivity: String {    
    case start  = "start"
    case stop   = "stop"
}
